INTRODUCTION
------------
Upcoming Content is a great way to display content that has a release date and 
needs to be displayed in groups of when it will be released.

REQUIREMENTS
------------
This module requires the following modules:
 * Chaos tool suite (https://drupal.org/project/ctools)
 * Date (https://drupal.org/project/date)
 * Date Views - part of the Date project in D7
 * Image (https://drupal.org/project/image)
 * Page Manager - part of the Chaos tool suite in D7
 * Panels (https://drupal.org/project/panels)
 * Views (https://drupal.org/project/views)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
* Enable the module ( `drush en -y upcoming_content` )
* In the page manager ( /admin/structure/pages ), in a page's variant, under 
content, click "Add Content" from the contextual menu link in a panel**
* Click on the Miscellaneous category
* Click the "Upcoming Content Pane" item
* Configure your options
* Save the form, and save the page's form
* View your page and see a list of upcoming content!

** To create a page, click the "add custom page" link from the page manager 
(/admin/structure/pages). To create a variant if you do not yet have one, 
click the "Add variant" tab from your page's edit page.

Some details about configuring the pane:
The content type to display must have at least an image and date field.  The
image field will be shown when the pane is rendered along with the title.  The 
date field will be used to group & filter the nodes by.  You may think of this 
as an "available on" date.

MAINTAINERS
-----------
 * Chris Svajlenka (svajlenka) - https://www.drupal.org/user/1813622
