<?php
/**
 * @file
 * This is the ctools content_type pane.
 *
 * All code related to the pane is contained here.
 */

/**
 * Define the ctools plugin and its defaults.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Upcoming Content Pane'),
  'description' => t('Display a list of upcoming nodes.'),
  'category' => t('Miscellaneous'),
  'edit form' => 'upcoming_content_upcoming_content_pane_edit_form',
  'render callback' => 'upcoming_content_upcoming_content_pane_render',
  'admin info' => 'upcoming_content_upcoming_content_pane_admin_info',
  'defaults' => array(
    'content_type' => '',
    'time_period' => 'week',
    'time_field' => '',
    'show_current' => '1',
    'num_time_periods' => '3',
    'nodes_per_period' => '3',
    'image_field' => '',
    'image_style' => '',
    'uid' => REQUEST_TIME,
  ),
);

/**
 * Admin info callback for panel pane.
 */
function upcoming_content_upcoming_content_pane_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';

    return $block;
  }
}

/**
 * Edit form callback for the content type.
 */
function upcoming_content_upcoming_content_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Fetch the available Content Types for use as the available options.
  $ct_options = array();
  $content_types = node_type_get_types();
  if (!empty($content_types)) {
    foreach ($content_types as $content_type) {
      $ct_options[$content_type->type] = $content_type->name;
    }
  }
  $default_ct_values = NULL;

  // Allow for selecting the content types that will be displayed in the pane.
  if (isset($conf['content_type'])) {
    $default_ct_values = $conf['content_type'];
  }
  // Fallback to defaults.
  if (empty($default_ct_values) && !empty($form_state['plugin']['defaults']['content_type'])) {
    $default_ct_values = $form_state['plugin']['defaults']['content_type'];
  }
  // If no defaults, fallback to first item.
  if (empty($default_ct_values)) {
    reset($array);
    $default_ct_values = key($ct_options);
  }

  $form['content_type'] = array(
    '#title' => t('Content Type'),
    '#description' => t('Select the content types that will be displayed.'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#options' => $ct_options,
    '#default_value' => $default_ct_values,
    '#required' => TRUE,
    '#ajax' => array(
      'path' => 'admin/upcoming_content_get_bundle_fields',
    ),
  );

  $default_image_field = $form_state['plugin']['defaults']['image_field'];
  // Allow for selecting the image field that will be displayed in the pane.
  if (isset($conf['image_field'])) {
    $default_image_field = $conf['image_field'];
  }

  $form['image_field'] = array(
    '#title' => t('Image Field'),
    '#description' => t('Set the image field to be used on the thumbnails.'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#default_value' => $default_image_field,
    '#required' => TRUE,
    '#validated' => 'true',
    '#options' => upcoming_content_get_image_fields($default_ct_values),
    '#prefix' => '<div id="image_field-replace">',
    '#suffix' => '</div>',
  );

  // Allow for selecting the image style that will be displayed in the pane.
  if (isset($conf['image_style'])) {
    $default_image_style = $conf['image_style'];
  }
  else {
    $default_image_style = $form_state['plugin']['defaults']['image_style'];
  }
  // Fetch the available Content Types for use as the available options.
  $image_style_options = array();
  $image_styles = image_styles();
  if (!empty($image_styles)) {
    foreach ($image_styles as $image_style) {
      $image_style_options[$image_style['name']] = $image_style['name'];
    }
  }
  $form['image_style'] = array(
    '#title' => t('Image Style'),
    '#description' => t('Select the image style to be used on the thumbnails.'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#options' => $image_style_options,
    '#default_value' => $default_image_style,
    '#required' => TRUE,
  );

  // Allow for selecting the timefield that will be used for filtering.
  $default_time_field = $form_state['plugin']['defaults']['time_field'];
  if (isset($conf['time_field'])) {
    $default_time_field = $conf['time_field'];
  }
  else {
    $default_time_field = $form_state['plugin']['defaults']['time_field'];
  }
  $form['time_field'] = array(
    '#title' => t('Time Field'),
    '#description' => t('Set the time field to be used to determine what is upcoming.'),
    '#type' => 'select',
    '#options' => upcoming_content_get_date_fields($default_ct_values),
    '#multiple' => FALSE,
    '#default_value' => $default_time_field,
    '#required' => TRUE,
    '#validated' => 'true',
    '#prefix' => '<div id="time_field-replace">',
    '#suffix' => '</div>',
  );

  // Time period type.
  $default_time_period = $form_state['plugin']['defaults']['time_period'];
  if (isset($conf['time_period']) && $conf['time_period']) {
    $default_time_period = $conf['time_period'];
  }
  $form['time_period'] = array(
    '#title' => t('Length of time period'),
    '#description' => t('The length of time period to group upcoming nodes by.'),
    '#type' => 'select',
    '#options' => array(
      'day' => t('Day'),
      'week' => t('Week'),
      'month' => t('Month'),
      'year' => t('Year'),
    ),
    '#default_value' => $default_time_period,
    '#required' => TRUE,
  );

  // Number of time periods.
  $default_num_time_periods = $form_state['plugin']['defaults']['num_time_periods'];
  if (isset($conf['num_time_periods']) && $conf['num_time_periods']) {
    $default_num_time_periods = $conf['num_time_periods'];
  }
  $form['num_time_periods'] = array(
    '#title' => t('Number of time periods displayed'),
    '#description' => t('The number of time periods to display'),
    '#type' => 'textfield',
    '#default_value' => $default_num_time_periods,
    '#required' => TRUE,
    '#size' => 3,
  );

  // Ability to set the number of results shown per time period.
  $default_nodes_per_period = $form_state['plugin']['defaults']['nodes_per_period'];
  if (isset($conf['nodes_per_period']) && $conf['nodes_per_period']) {
    $default_nodes_per_period = $conf['nodes_per_period'];
  }
  $form['nodes_per_period'] = array(
    '#title' => t('Number of Nodes per time period'),
    '#description' => t('The number of results shown initially per time period.'),
    '#type' => 'textfield',
    '#default_value' => $default_nodes_per_period,
    '#required' => TRUE,
    '#size' => 3,
  );

  // Whether or not to include the current time period.
  $form['show_current'] = array(
    '#title' => t('Show Current Time Period'),
    '#description' => t('Show nodes in the current time period as well as upcoming?'),
    '#type' => 'checkbox',
  );
  if (isset($conf['show_current']) && $conf['show_current']) {
    $form['show_current']['#attributes'] = array('checked' => 'checked');
  }

  $default_uid = $form_state['plugin']['defaults']['uid'];
  if (isset($conf['uid']) && $conf['uid']) {
    $default_uid = $conf['uid'];
  }
  $form['uid'] = array(
    '#type' => 'hidden',
    '#default_value' => $default_uid,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function upcoming_content_upcoming_content_pane_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type).
 */
function upcoming_content_upcoming_content_pane_render($subtype, $conf, $panel_args, $context = NULL) {
  $block = new stdClass();

  // Initial content is blank.
  $block->title = '';
  $block->content = '';

  // Check for needed config.
  if (!$conf['num_time_periods'] || !$conf['nodes_per_period']) {
    return $block;
  }

  $has_nodes = FALSE;
  $time_periods = array();
  $total_rows = array();

  $name_formats = array(
    'day' => 'upcoming_content_day',
    'week' => 'upcoming_content_week',
    'month' => 'upcoming_content_month',
    'year' => 'upcoming_content_year',
  );
  $name_format = $name_formats[$conf['time_period']];

  $offset = 0;
  if ($conf['show_current'] == 0) {
    $offset = 1;
  }

  // Iterate over the number of time periods to display.
  for ($i = 0; $i < $conf['num_time_periods']; $i++) {
    $current_offset = $i + $offset;
    $s1 = ($current_offset <= 1) ? '' : 's';
    $time = 'now + ' . $current_offset . ' ' . $conf['time_period'] . $s1;
    if ($current_offset == 0) {
      $time = 'now';
    }

    // Grab the nodes for this time period.
    module_load_include('inc', 'upcoming_content', 'includes/upcoming_content.views');
    $rtn = upcoming_content_fetch_nodes($conf, $current_offset);
    $time_periods[$i] = array();
    if (!empty($rtn['nodes'])) {
      foreach ($rtn['nodes'] as $node) {
        $time_periods[$i][] = drupal_render($node);
      }
    }
    $total_rows[$i] = $rtn['total_rows'];

    $time_data[$i] = format_date(strtotime($time), $name_format);

    // If no nodes found, nothing to see here, move along.
    if (empty($time_periods[$i])) {
      continue;
    }
    $has_nodes = TRUE;
  }

  // No upcoming nodes!
  if (!$has_nodes) {
    return $block;
  }

  // Allow for standard use of the override title.
  if ($conf['override_title'] && $conf['override_title_text']) {
    $block->title = $conf['override_title_text'];
  }

  $block->content = array(
    '#theme' => 'upcoming_content_content_wrapper',
    '#conf' => $conf,
    '#time_periods' => $time_periods,
    '#time_data' => $time_data,
  );

  // Pass the current offset and the total_rows on to the pane's conf value.
  // This is for use with the AJAX requests.
  $conf['offset'] = count($rtn['nodes']);
  $conf['total'] = $total_rows;

  return $block;
}
