<?php
/**
 * @file
 * This is the wrapper template for upcoming content.
 *
 * All code related to the pane is contained here.
 * @time_periods - the time periods to display.
 * @conf - The pane's configuration.
 * @time_data - the time peroid's name.
 */
?>

<section class="upcoming-pane" id="browse-<?php print $conf['uid']; ?>" data-uid="<?php print $conf['uid']; ?>">
  <div id="ajax-area-wrapper-<?php print $conf['uid']; ?> ">
    <?php if (!empty($time_periods)): ?>
      <?php foreach ($time_periods as $time_period => $nodes): ?>
        <ul class="upcoming-time-period">
          <span class="time-period"><?php print $time_data[$time_period]; ?></span>
          <?php if (!empty($nodes)): ?>
            <?php foreach ($nodes as $node): ?>
            <li>
              <?php print $node; ?>
            </li>
            <?php endforeach; ?>
          <?php endif; ?>
        </ul>
      <?php endforeach; ?>
    <?php endif; ?>
    <div class="clear"></div>
  </div>
</section>
