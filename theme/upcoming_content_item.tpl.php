<?php
/**
 * @file
 * This is the template for upcoming content.
 *
 * @conf - the pane's configuration.
 * @node - the node.
 * @link - a link to the node.
 * @image - a rendered image for the node.
 * @time - the time this node is available.
 */
?>
<div class="upcoming-item">
  <div class="item-image">
    <?php if (!empty($image)): ?>
      <?php print $image; ?>
    <?php endif; ?>
    <div class="dimimage"></div>
  </div>
  <div class="item-details">
  <?php if (!empty($link)): ?>
    <div class="title">
      <?php print $link; ?>
    </div>
  <?php endif; ?>
  <?php if (!empty($time)): ?>
    <div class="available-date">
      <?php print $time; ?>
    </div>
  </div>
<?php endif; ?>
</div>
