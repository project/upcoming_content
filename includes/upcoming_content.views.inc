<?php
/**
 * @file
 * The views-portion of upcoming_content.
 *
 * All views related methods here.
 */

/**
 * Grabs nodes given the configuration and offset.
 */
function upcoming_content_fetch_nodes($conf, $offset = 0, $filters = '') {

  $s1 = ($offset <= 1) ? '' : 's';
  $conf['start_time'] = 'now + ' . $offset . ' ' . $conf['time_period'] . $s1;
  if ($offset == 0) {
    $conf['start_time'] = 'now';
  }

  $view = upcoming_content_gen_view($conf);
  $view->items_per_page = $conf['nodes_per_period'];
  $options = array();
  // If no content type filter was applied already.
  // Fall back on the default allowed types for this pane.
  if (!empty($conf['content_type'])) {
    $options['value'] = array($conf['content_type']);
    $view->set_item('default', 'filter', 'type', NULL);
    $view->add_item('default', 'filter', 'node', 'type', $options);
  }

  $pager = $view->display_handler->get_option('pager');
  $pager['type'] = 'some';
  $view->display_handler->set_option('pager', $pager);

  // Execute the view allowing for pre and post execution tasks.
  $view->pre_execute();
  $view->execute();
  $view->post_execute();

  // If nodes were found, loop through them and render them.
  $nodes = array();

  if (!empty($view->result)) {
    foreach ($view->result as $node) {
      $link = l($node->node_title, 'node/' . $node->nid);
      $image = NULL;
      if (!empty($node->field_image[0]['rendered'])) {
        $image = drupal_render($node->field_image[0]['rendered']);
      }
      $time = NULL;
      if (!empty($node->field_time[0]['rendered'])) {
        $time = drupal_render($node->field_time[0]['rendered']);
      }
      $nodes[$node->nid] = array(
        'content' => array(
          '#theme' => 'upcoming_content_content_item',
          '#conf' => $conf,
          '#node' => $node,
          '#link' => $link,
          '#image' => $image,
          '#time' => $time,
        ),
      );
    }
  }

  // Return the nodes and the total rows for determining if load more is ok.
  $rtn = array('nodes' => $nodes, 'total_rows' => $view->total_rows);

  return $rtn;
}

/**
 * Generates our view for upcoming content.
 */
function upcoming_content_gen_view($conf) {
  $view = views_new_view();
  $view->name = 'upcoming_content_view';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Upcoming Content';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';

  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;

  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;

  /* Field: Content: type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['type']['alter']['ellipsis'] = FALSE;

  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  if (module_exists('domain')) {
    /* Filter criterion: Domain Access: Available on current domain */
    $handler->display->display_options['filters']['current_all']['id'] = 'current_all';
    $handler->display->display_options['filters']['current_all']['table'] = 'domain_access';
    $handler->display->display_options['filters']['current_all']['field'] = 'current_all';
    $handler->display->display_options['filters']['current_all']['value'] = '1';
    $handler->display->display_options['filters']['current_all']['group'] = 1;
  }

  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array();
  $handler->display->display_options['filters']['type']['group'] = 1;

  if ($conf['image_field'] && $conf['image_style']) {
    /* Field: Content: Home Page Thumbnail */
    $handler->display->display_options['fields']['image']['id'] = $conf['image_field'];
    $handler->display->display_options['fields']['image']['table'] = 'field_data_' . $conf['image_field'];
    $handler->display->display_options['fields']['image']['field'] = $conf['image_field'];
    $handler->display->display_options['fields']['image']['label'] = '';
    $handler->display->display_options['fields']['image']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['image']['element_default_classes'] = FALSE;
    $handler->display->display_options['fields']['image']['click_sort_column'] = 'fid';
    $handler->display->display_options['fields']['image']['settings'] = array(
      'image_style' => $conf['image_style'],
      'image_link' => 'content',
    );
  }

  // Filter on whatever field we're filtering by.
  if ($conf['time_field'] && $conf['start_time'] && $conf['time_period']) {

    $key = 'time';
    $table = 'field_data_' . $conf['time_field'];

    // Field for the time.
    $handler->display->display_options['fields'][$key]['id'] = $conf['time_field'];
    $handler->display->display_options['fields'][$key]['table'] = $table;
    $handler->display->display_options['fields'][$key]['field'] = $conf['time_field'];
    $handler->display->display_options['fields'][$key]['label'] = '';
    $handler->display->display_options['fields'][$key]['settings'] = array(
      'format_type' => 'short',
      'fromto' => 'both',
      'multiple_number' => '',
      'multiple_from' => '',
      'multiple_to' => '',
    );

    /* Filter criterion: Content: Type */
    $key = 'time_filter';
    $handler->display->display_options['filters'][$key]['id'] = $conf['time_field'] . '_value';
    $handler->display->display_options['filters'][$key]['table'] = $table;
    $handler->display->display_options['filters'][$key]['field'] = $conf['time_field'] . '_value';

    $formats = array(
      'day' => 'Y-m-d',
      'week' => 'Y-\WW',
      'month' => 'Y-m',
      'year' => 'Y',
    );
    $format = $formats[$conf['time_period']];
    $range = upcoming_content_get_time_period_range($conf['time_period'], strtotime($conf['start_time']));
    $handler->display->display_options['filters'][$key]['value'] = array(
      'min' => date($format, strtotime($range['start_time'])),
      'max' => date($format, strtotime($range['end_time'])),
      'value' => date($format, strtotime($range['start_time'])),
      'type' => 'date',
    );

    $handler->display->display_options['filters'][$key]['group'] = 1;
    $handler->display->display_options['filters'][$key]['granularity'] = $conf['time_period'];
    $handler->display->display_options['filters'][$key]['year_range'] = '-0:+10';

    /* Sort criterion: Content: time field */
    $handler->display->display_options['sorts'][$key]['id'] = $key;
    $handler->display->display_options['sorts'][$key]['table'] = $table;
    $handler->display->display_options['sorts'][$key]['field'] = $key;
    $handler->display->display_options['sorts'][$key]['order'] = 'ASC';
  }

  return $view;
}
